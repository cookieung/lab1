package oop.lab1;


/*
 * Student is a Student Class for build a person object from name and id. And inherit from person class.
 * @author Salilthip Phuklang
 */
public class Student extends Person {
	private long id;
	
/*
 * For initialize name and id.
 ** @author Salilthip Phuklang
 */
	public Student(String name, long id) {
		super(name); // name is managed by Person
		this.id = id;
	}

	/** return a string representation of this Student. */
	public String toString() {
		return String.format("Student %s (%d)", getName(), id);
	}

	public boolean equals(Person obj) {
		if (obj == null) return false;
		if ( obj.getClass() != this.getClass() ) return false;
		Student other = (Student) obj;
		if ( this.id == other.id ) return true;
		return false;
	}
}
